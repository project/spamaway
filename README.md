INTRODUCTION
------------

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/spamaway

REQUIREMENTS
------------

This module requires the following modules:
* Webform

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/895232/ for further information.

CONFIGURATION
-------------

* Add the handler to the required webforms.


MAINTAINERS
-----------

Current maintainers:
* Michael Schuddings - https://www.drupal.org/u/mschudders
